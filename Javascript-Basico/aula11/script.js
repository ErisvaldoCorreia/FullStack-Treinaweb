// desafio - menor numero do array

function min(numbers) {
    let min = numbers[0];
    for(let i = 0; i < numbers.length; i++) {
        if(numbers[i] < min) {
            min = numbers[i];
        }
    }

    return min;
}

var arr = [7, 12, 3, 4, 50, 1, 56, 34, 45];

console.log(
    min(arr)
)