let nota = 7;

if (nota > 8) {
    console.log('aprovado');
} else if (nota > 5) {
    console.log('recuperação');
} else {
    console.log('reprovado');
}


let escolha = 2;
switch(escolha) {
    case 1:
        console.log('escolheu 1');
        break;
    case 2:
        console.log('escolheu 2');
        break;
    case 3:
        console.log('escolheu 3');
        break;
    default:
        console.log('opções invalidas');
        break;
}

