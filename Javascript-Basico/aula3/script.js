// conteudo utilizando o Node.js para facilitar as visualizações

// operadores
console.log(5 + 6);
console.log(5 % 2);

console.log(3 > 6);
console.log(5 != 3);

console.log(3 < 4 && 7 < 5);

// operação ternaria
console.log(3 >= 2 ? 'Sim' : 'Não');

// operadores matematicos:
// * / + - % **

// operadores relacionais
// > >= < <= == != === !==

// operadores lógicos:
// && ||
