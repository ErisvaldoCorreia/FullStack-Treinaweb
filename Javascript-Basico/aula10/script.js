function somar(n1, n2, n3) {
    return n1 + n2 + n3;
}

var arr = [10, 20, 30];

console.log(
    sum(...arr)
);

// usamos o operador spread (...) para
// abrir o array na chamada da função!

var arr1 = [1,2,3,4,5];
var arr2 = [...arr1, 6, 7, 8, 9];

console.log(arr2);
