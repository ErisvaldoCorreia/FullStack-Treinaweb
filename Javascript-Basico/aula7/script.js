// exemplos de funções
function msg() {
    console.log('Isso imprime apenas uma mensagem!');
}

function somar(a, b) {
    return a + b;
}

let retorno = somar(10, 5);
console.log('Valor somado: ' + retorno);


// arrows functions
var valor = () => { 10 + 9 };

console.log(valor);
