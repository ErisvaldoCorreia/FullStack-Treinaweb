// referencias e declaração simplificada

var teste = [ 2, 4, 6 ];
var [a, b, c] = teste;
console.log(b);


var arr1 = [ 1, 2, 3 ];
var arr2 = arr1;

console.log(arr1 === arr2);
// deve retornar true

arr2[1] = 5;
console.log(arr1, arr2);

var arr3 = [ 1, 5, 3 ];
console.log(arr2 === arr3);
// deve retornar false pois sao iguais mas nao referenciam o mesmo objeto

