// declaração de variaveis

// escopo global:
let nome = 'Erisvaldo';

// com escopo local (funçao para exmeplo):
function ola() {
    let word = 'ola';
    console.log(nome);
    console.log(word);
}

// chamando a função exmeplo.
ola();

console.log(nome);
// console.log(word);
// se descomentar a linha acima, obteremos um erro na execução.


// constantes
const NOME = 'Valor fixo inalteravel';

// modificando o valor de uma variavel
nome = 'Junior';

console.log(nome);
