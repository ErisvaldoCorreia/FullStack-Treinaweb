// esse código tem por objetivo apresentar as declarações de variaveis

#include <stdio.h>
#include <stdlib.h>

int main() {
    /* <tipo variavel><nome variavel>[ = <valor inicial>]; */
    int num1;
    int num2 = 10;
    num1 = num2;
    num2 = 15;

    printf(num1);
    printf(num2);

    // getch cria uma pequena pausa antes do programa seguir!
    getch();
    return 0;
}
